#include <stdio.h>
#include <mem.h>

void concat(char s1[], char s2[]) {
    printf("Enter the first string: ");
    gets(s1);
    printf("Enter the second string: ");
    gets(s2);
    strcat(s1, " ");
    strcat(s1,s2);
    printf("The concatenated string: %s\n", s1);
}

int main() {
    char str1[100];
    char str2[100];
    concat(str1, str2);
    return 0;
}
